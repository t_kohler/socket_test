package concurrent;

import java.io.*;
import java.net.Socket;

class Handler implements Runnable {
    private final Socket socket;
    Handler(Socket socket) { this.socket = socket; }

    @Override
    public void run() {
        processConnection();
    }

    private void processConnection() {
        try {
            String message = readMessage(this.socket);
            System.out.println(Thread.currentThread().getName()+": "+message);
            message += " - from Thread: " + Thread.currentThread().getName();
            writeMessage(this.socket, message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String readMessage(Socket socket) throws IOException {
        BufferedReader bufferedReader =
                new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()));
        char[] buffer = new char[200];
        int numberOfChars = bufferedReader.read(buffer, 0, 200); // blockiert bis Nachricht empfangen
        String message = new String(buffer, 0, numberOfChars);
        return message;
    }

    void writeMessage(Socket socket, String message) throws IOException {
        PrintWriter printWriter =
                new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()));
        printWriter.print(message);
        printWriter.flush();
    }
}
