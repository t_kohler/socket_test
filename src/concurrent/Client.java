package concurrent;// sequential.Client.java

import java.io.*;
import java.net.Socket;
import java.util.Random;

public class Client implements Runnable{

    int id;
    NotifyObject nfo;

    public Client(int id, NotifyObject nfo){
        this.id = id;
        this.nfo = nfo;
    }

    @Override
    public void run() {
        try {
            test(this.id);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    void test(int i) throws IOException {
        String ip = "127.0.0.1"; // localhost
        int port = 11111;
        Socket socket = new Socket(ip,port); // verbindet sich mit sequential.Server
        String messageToSend = "Hello, world!";
        messageToSend += " - client["+i+"]";
        writeMessage(socket, messageToSend);
        nfo.doWait();
        String receivedMessage = readMessage(socket);
        System.out.println(receivedMessage);
    }
    void writeMessage(Socket socket, String message) throws IOException {
        PrintWriter printWriter =
                new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()));
        printWriter.print(message);
        printWriter.flush();
    }
    String readMessage(Socket socket) throws IOException {
        BufferedReader bufferedReader =
                new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()));
        char[] buffer = new char[200];
        int numberOfChars = bufferedReader.read(buffer, 0, 200); // blockiert bis Nachricht empfangen
        String message = new String(buffer, 0, numberOfChars);
        return message;
    }
}