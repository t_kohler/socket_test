package concurrent;

import java.io.IOException;
import java.util.Random;

public class ServerApp {

    public static void main(String[] args){
        int poolSize = 5;
        int port = 11111;
        try {
            Thread thread = new Thread(new NetworkService(port, poolSize));
            thread.start();
            NotifyObject nfo = new NotifyObject();
            for(int i = 0; i<5;i++){
                Thread t = new Thread(new Client(i,nfo));
                t.start();
            }
            try {
                Thread.sleep(5000);
                nfo.doNotify();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
