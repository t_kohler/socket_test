package sequential;// sequential.Server.java

import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;

public class Server {
    public static void main(String[] args) {
        Server server = new Server();
        try {
            server.test();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void test() throws IOException {
        int port = 11111;
        ServerSocket serverSocket = new ServerSocket(port);
        Socket client = waiteForSigneup(serverSocket);
        String message = readMessage(client);
        System.out.println(message);
        message += " - server";
        writeMessage(client, message);
    }

    Socket waiteForSigneup(ServerSocket serverSocket) throws IOException {
        Socket socket = serverSocket.accept(); // blockiert, bis sich ein sequential.Client angemeldet hat
        return socket;
    }

    String readMessage(Socket socket) throws IOException {
        BufferedReader bufferedReader =
                new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()));
        char[] buffer = new char[200];
        int numberOfChars = bufferedReader.read(buffer, 0, 200); // blockiert bis Nachricht empfangen
        String message = new String(buffer, 0, numberOfChars);
        return message;
    }

    void writeMessage(Socket socket, String message) throws IOException {
        PrintWriter printWriter =
                new PrintWriter(
                        new OutputStreamWriter(
                                socket.getOutputStream()));
        printWriter.print(message);
        printWriter.flush();
    }
}